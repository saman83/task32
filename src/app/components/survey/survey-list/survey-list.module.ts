import { SurveyListItemComponent } from './../survey-list-item/survey-list-item.component';
import { SurveyListComponent } from './survey-list.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import {  } from '../survey-list-item/survey-list-item.component'

const routes: Routes = [
    {
        path: '',
        component: SurveyListComponent
    }
];



@NgModule({
    declarations: [ SurveyListItemComponent ],
    imports: [ RouterModule.forChild( routes ) ],
    exports: []
})
export class SurveyListModule {}
